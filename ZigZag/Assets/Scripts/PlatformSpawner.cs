﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformSpawner : MonoBehaviour {

    public static PlatformSpawner instance;

    public GameObject platform;
    public GameObject powerup1;
    Vector3 recentPos;
    float size;

    public bool gameOver;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

    }

    // Use this for initialization
    void Start ()
    {
        recentPos = platform.transform.position;
        size = platform.transform.localScale.x; 

        
       
        

        

	}


	
	// Update is called once per frame
	void Update ()
    {
        if (gameOver)
        {
            CancelInvoke("SpawnPlatforms");
        }
    }

    //Randomly Spawn Random Platforms
    void SpawnPlatforms()
    {
        
        int random = Random.Range(0, 11);

        if (random < 5)
        {
            SpawnX();
        }
        else if (random >= 5)
        {
            SpawnZ();
        }

    }

    public void StartSpawning()
    {
        InvokeRepeating("SpawnPlatforms", 0.2f, 0.2f);
    }

    //Spawn Platforms at (X, Z)
    void SpawnX()
    {
        Vector3 pos = recentPos;
        pos.x += size;

        Instantiate(platform, pos, Quaternion.identity/*No rotation*/);

        

        recentPos = pos;


        //GEnerate random to "Randomly" create pearls 
        int rand = Random.Range(0, 4);
        if (rand < 2)
        {
            pos.y += 1;
            Instantiate(powerup1, pos, powerup1.transform.rotation);

        }



        
    }

    void SpawnZ()
    {
        Vector3 pos = recentPos;
        pos.z += size;

        Instantiate(platform, pos, Quaternion.identity/*No rotation*/);

        recentPos = pos;

        int rand = Random.Range(0, 4);
        if (rand < 2)
        {
            pos.y += 1;
            Instantiate(powerup1, pos, powerup1.transform.rotation);

        }


    }
}
