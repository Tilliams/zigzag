﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public GameObject player;
    public float lerpRate;
    Vector3 offset;
    public bool gameOver = false;



	// Use this for initialization
	void Start ()
    {
        offset = player.transform.position - transform.position;

        gameOver = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameOver)
        {
            Suit();
        }

    }


    //Camera Follow player
    void Suit()
    {
        //Current Camera position
        Vector3 pos = transform.position;

        //Position We want it to be at
        Vector3 goalPos = player.transform.position - offset;

        //Changes position; TIme.delta helps keep it smooth on ALL platforms
        pos = Vector3.Lerp (pos, goalPos, lerpRate * Time.deltaTime);

        transform.position = pos;

        
    }
}
