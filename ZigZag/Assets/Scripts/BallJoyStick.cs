﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BallJoyStick : MonoBehaviour {

    [SerializeField]
    private float speed;

    public GameObject effect1;
    //public AudioSource audioData;

    bool started;
    bool gameOver;

    //Setup Rigidbody object
    Rigidbody rbody;

    

    void Awake()
    {
        //Allows us to control our ball
        rbody = GetComponent<Rigidbody>();
        //audioData = GetComponent<AudioSource>();
    }
    // Use this for initialization
    void Start () {
        started = false;
        gameOver = false;
    }
	
	// Update is called once per frame
	void Update () {


        if (!started)
        {
            //Processes Mouse Down button
            if (Input.GetMouseButtonDown(0))
            {
                //Initalives velocity using a Vector3 Object
                rbody.velocity = new Vector3(speed, 0, 0);
                started = true;

                GameManager.instance.StartGame();
                

            }
        }

        //Original position of ball, Ray goes downward, size
        //Returns true when colliding with an object

        //Debug.DrawRay(transform.position, Vector3.down, Color.red);
        if (!Physics.Raycast(transform.position, Vector3.down, 1f))
        {

            gameOver = true;
            rbody.velocity = new Vector3(0, -25f, 0);
            //cam.GetComponent<CameraFollow> ().gameOver = true;
            ScoreManager.instance.StopScore();
            GameManager.instance.GameOver();
            
            


        }

        if (Input.GetMouseButtonDown(0) && !gameOver)
        {
            ChangeDirection();
        }
        
		
	}

    void ChangeDirection()
    {
        //If the z velocity is greater than 0
        if (rbody.velocity.z > 0)
        {
            //Change the z velocity to 0 and x velocity to speed
            rbody.velocity = new Vector3(speed, 0, 0);

        }

        //Do the opposite
        else if (rbody.velocity.x > 0)
        {
            rbody.velocity = new Vector3(0, 0, speed);
        }

        /*
        else if (rigidbody.velocity.x < 0)
        {
            rigidbody.velocity = new Vector3(0, 0, -speed);
        }

        else if (rigidbody.velocity.z < 0)
        {
            rigidbody.velocity = new Vector3(speed, 0, 0);
        }
        */
    }

    void OnTriggerEnter(Collider col)

    {
        if(col.gameObject.tag == "Pearl")
        {
            //audioData.Play();
            GameObject temp = Instantiate(effect1, col.gameObject.transform.position, Quaternion.identity) as GameObject;
            Destroy(col.gameObject);
            Destroy(temp, 1f);

        }
    }
}
