﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {

    public static ScoreManager instance;
    public int score, highscore;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

    }



    // Use this for initialization
    void Start() {

        score = 0;
        PlayerPrefs.SetInt("Score", score);

    }

    // Update is called once per frame
    void Update() {

    }

    void IncreaseScore()
    {
        score = score + 1;

    }

    public void StartScore()
    {
        InvokeRepeating("IncreaseScore", 0.1f, 0.5f);
    }

    public void StopScore()
    {
        CancelInvoke();
        PlayerPrefs.SetInt("Score", score);

        if (PlayerPrefs.HasKey("High Score"))
        {
            if (score > PlayerPrefs.GetInt("High Score"))
                {

                PlayerPrefs.SetInt("High Score", score);
            }

        }

        else
        {
            PlayerPrefs.SetInt("High Score", score);
        }

    }
}
