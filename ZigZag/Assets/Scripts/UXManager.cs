﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UXManager : MonoBehaviour {


    public static UXManager instance;
    public GameObject TableauPanel;
    public GameObject GameOverPanel;
    public GameObject TapText;
    public Text StartHighScore;
    public Text YourScore1;
    public Text HighScore2;


    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }


    // Use this for initialization
    void Start ()
    {
        
        StartHighScore.text = "High Score: " +PlayerPrefs.GetInt("High Score").ToString();

    }

    public void GameStart()
    {
        TapText.SetActive(false);
        TableauPanel.GetComponent<Animator>().Play("PanelVanish");
        

    }

    public void GameEnd()
    {
        GameOverPanel.SetActive(true);
        GameOverPanel.GetComponent<Animator>().Play("GameOver");
        HighScore2.text = PlayerPrefs.GetInt("High Score").ToString();


        YourScore1.text = PlayerPrefs.GetInt("Score").ToString();


    }

    public void Retry()
    {
        SceneManager.LoadScene("ZigZagLevel1");

    }

    // Update is called once per frame
    void Update () {
		
	}
}
